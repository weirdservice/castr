module.exports = [
  'scrapr:isUpdated',
  'scrapr:scrap',
  'postr:exists',
  'postr:insert',
  'registr:exists',
  'registr:insert',
  'schedulr:find',
  'schedulr:scrapped'
]
