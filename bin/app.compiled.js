#!/usr/bin/env node
'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _nodeRedisPubsub = require('node-redis-pubsub');

var _nodeRedisPubsub2 = _interopRequireDefault(_nodeRedisPubsub);

var _castees = require('./castees');

var _castees2 = _interopRequireDefault(_castees);

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).parse(process.argv);

var config = { port: 6379, scope: 'test' },
    nrp = new _nodeRedisPubsub2['default'](config);

// for save status
var castr = {};

var parseType = function parseType(channel, key) {
  return channel.split(':castr:' + key + ':')[1];
};

var add = function add(data, channel) {
  var type = parseType(channel, 'add');
  castr[type] = castr[type] || [];
  var idx = castr[type].indexOf(data.id);
  if (idx !== -1) return;
  castr[type].push(data.id);
  console.log('add', type, data.id);
};

var remove = function remove(data, channel) {
  var type = parseType(channel, 'remove');
  castr[type] = castr[type] || [];
  var idx = castr[type].indexOf(data.id);
  if (idx == -1) return;
  castr[type].splice(idx, 1);
  console.log('remove', type, data.id);
};

var popCast = function popCast(type) {
  return castr[type][Math.floor(Math.random() * castr[type].length)];
};

nrp.on('castr:add:*', add);
nrp.on('castr:remove:*', remove);

_castees2['default'].forEach(function (castee) {
  var _castee$split = castee.split(':');

  var _castee$split2 = _slicedToArray(_castee$split, 2);

  var type = _castee$split2[0];
  var action = _castee$split2[1];

  nrp.on(castee, function (data) {
    var key = popCast(type);
    console.log(castee + ':' + key);
    nrp.emit(castee + ':' + key, data);
  });
});

process.on('SIGINT', function () {
  console.log("\nBye!");
  nrp.quit();
  process.exit();
});

console.log('Listen castr:*');

// norty code for intial register
setTimeout(function () {
  nrp.emit('castr:callback', {});
}, 100);
