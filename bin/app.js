#!/usr/bin/env node
import program from 'commander'
import NRP from 'node-redis-pubsub'

import castees from './castees'
import pkg from '../package.json'

program
  .version(pkg.version)
  .parse(process.argv)

const config = { port: 6379, scope: 'test' },
      nrp = new NRP(config)

// for save status
var castr = {}

const parseType = (channel, key) => {
  return channel.split(`:castr:${key}:`)[1]
}

const add = (data, channel) => {
  let type = parseType(channel, 'add')
  castr[type] = castr[type] || []
  let idx = castr[type].indexOf(data.id)
  if(idx !== -1) return;
  castr[type].push(data.id)
  console.log('add', type, data.id)
}

const remove = (data, channel) => {
  let type = parseType(channel, 'remove')
  castr[type] = castr[type] || []
  let idx = castr[type].indexOf(data.id)
  if(idx == -1) return;
  castr[type].splice(idx, 1)
  console.log('remove', type, data.id)
}

const popCast = (type) => {
    return castr[type][Math.floor(Math.random() * castr[type].length)]
}

nrp.on('castr:add:*', add)
nrp.on('castr:remove:*', remove)

castees.forEach((castee) => {
  let [type, action] = castee.split(':')
  nrp.on(castee, (data) => {
    let key = popCast(type)
    console.log(castee + ':' + key)
    nrp.emit(castee + ':' + key, data)
  })
})

process.on('SIGINT', () => {
  console.log("\nBye!")
  nrp.quit()
  process.exit()
})

console.log('Listen castr:*')

// norty code for intial register
setTimeout(() => {
  nrp.emit('castr:callback', {})
}, 100)
